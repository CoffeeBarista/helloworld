<?php

use App\Controllers\HomeController;
use App\Controllers\ListenerController;
use App\Factory\Couchbase;
use App\Factory\CouchbaseInterface;
use App\Factory\VisualPing as VisualPingFactory;
use App\Factory\VisualPingInterface as VisualPingFactoryInterface;
use App\Middlewares\RecordMessage;
use App\Middlewares\Reference as MiddlewareReference;
use App\Middlewares\Translators\VisualPing as MiddlewareVisualPing;
use App\Providers\Reference as ProviderReference;
use App\Providers\ReferenceInterface;
use App\Providers\Translators\VisualPing as VisualPingProvider;
use App\Providers\Translators\VisualPingInterface as VisualPingProviderInterface;
use App\Services\Translators\VisualPing as VisualPingTranslator;
use App\Services\Translators\VisualPingInterface as VisualPingTranslatorInterface;
use DI\Container;
use Laminas\Diactoros\RequestFactory;
use Laminas\Diactoros\ResponseFactory;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidFactory;

require __DIR__ . '/../vendor/autoload.php';

use App\Services\Repository\CouchbaseInterface as CouchbaseRepositoryInterface;
use App\Services\Repository\Couchbase as CouchbaseRepository;
use Ramsey\Uuid\UuidFactoryInterface;

$container = new Container([
    RequestFactoryInterface::class => new RequestFactory(),
    ResponseFactoryInterface::class => new ResponseFactory(),

    // Providers
    ReferenceInterface::class => new ProviderReference(),

    // Factories
    VisualPingFactoryInterface::class => new VisualPingFactory(),
    VisualPingProviderInterface::class => new VisualPingProvider(),
    CouchbaseInterface::class => new Couchbase(),
    UuidFactoryInterface::class => new UuidFactory(),
]);

// Services
$container->set(VisualPingTranslatorInterface::class, function (Container $container) {
    return new VisualPingTranslator(
        $container->get(VisualPingFactoryInterface::class),
    );
});

$container->set(CouchbaseRepositoryInterface::class, function (Container $container) {
    return new CouchbaseRepository(
        $container->get(CouchbaseInterface::class),
        $container->get(UuidFactoryInterface::class),
    );
});

// Middlewares
$container->set(MiddlewareReference::class, function (Container $container) {
    return new MiddlewareReference(
        $container->get(ReferenceInterface::class),
    );
});

$container->set(MiddlewareVisualPing::class, function (Container $container) {
    return new MiddlewareVisualPing(
        $container->get(VisualPingTranslatorInterface::class),
        $container->get(VisualPingProviderInterface::class),
        $container->get(ResponseFactoryInterface::class),
    );
});

$container->set(RecordMessage::class, function (Container $container) {
    return new RecordMessage(
        $container->get(CouchbaseRepositoryInterface::class),
        $container->get(ReferenceInterface::class),
        $container->get(VisualPingProviderInterface::class),
        $container->get(ResponseFactoryInterface::class),
    );
});

// Controllers
$container->set('HomeController', function (Container $container) {
    return new HomeController($container->get(CouchbaseInterface::class));
});

$container->set('ListenerControllerInterface', function (Container $container) {
    return new ListenerController($container->get(ReferenceInterface::class));
});
