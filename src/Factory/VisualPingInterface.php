<?php

namespace App\Factory;

use App\Models\VisualPingInterface as VisualPingInterfaceModel;

interface VisualPingInterface
{
    public function create(): VisualPingInterfaceModel;
}
