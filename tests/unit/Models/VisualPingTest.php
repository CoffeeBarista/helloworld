<?php

namespace Tests\Unit\Models;

use App\Models\VisualPing;
use App\Models\VisualPingInterface;
use PHPUnit\Framework\TestCase;

final class VisualPingTest extends TestCase
{
    public function testInterface(): void
    {
        $sut = new VisualPing();
        self::assertInstanceOf(VisualPingInterface::class, $sut);
    }

    public function testProvider(): void
    {
        $test = 'testString';
        $sut = new VisualPing();

        self::assertNull($sut->getUrl());
        $sut->setUrl($test);
        self::assertSame($test, $sut->getUrl());

        self::assertNull($sut->getOriginalView());
        $sut->setOriginalView($test);
        self::assertSame($test, $sut->getOriginalView());

        self::assertNull($sut->getPreviewView());
        $sut->setPreviewView($test);
        self::assertSame($test, $sut->getPreviewView());

        self::assertSame([
            'url' => 'testString',
            'original_view' => 'testString',
            'preview_view' => 'testString',
        ], $sut->getArray());
    }
}
