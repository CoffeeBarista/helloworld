<?php

namespace App\Factory;

use App\Models\VisualPing as VisualPingModel;
use App\Models\VisualPingInterface as VisualPingInterfaceModel;

final class VisualPing implements VisualPingInterface
{
    public function create(): VisualPingInterfaceModel
    {
        return new VisualPingModel();
    }
}
