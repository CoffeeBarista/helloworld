<?php

namespace App\Middlewares;

use App\Providers\ReferenceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class Reference implements MiddlewareInterface
{
    public function __construct(
        private readonly ReferenceInterface $referenceProvider,
    ) {
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler,
    ): ResponseInterface {
        // Avoiding getting static route context - as not unit testable
        $results = explode('/', $request->getRequestTarget());
        $reference = array_pop($results);

        $this->referenceProvider->setReference($reference);

        return $handler->handle($request);
    }
}
