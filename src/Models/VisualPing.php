<?php

namespace App\Models;

final class VisualPing implements VisualPingInterface
{
    private ?string $url = null;
    private ?string $originalViewUrl = null;
    private ?string $previewViewlUrl = null;

    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setOriginalView(string $url): void
    {
        $this->originalViewUrl = $url;
    }

    public function getOriginalView(): ?string
    {
        return $this->originalViewUrl;
    }

    public function setPreviewView(string $url): void
    {
        $this->previewViewlUrl = $url;
    }

    public function getPreviewView(): ?string
    {
        return $this->previewViewlUrl;
    }

    public function getArray(): array
    {
        return [
            'url' => $this->url,
            'original_view' => $this->originalViewUrl,
            'preview_view' => $this->previewViewlUrl,
        ];
    }
}
