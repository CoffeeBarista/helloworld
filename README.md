# VisualPing Email Project

This project will listen on /listener/{context} for VisualPing's web hook https://help.visualping.io/en/articles/6191684-how-do-i-use-webhooks
Using the body of data to dispatch a email to a user with a before and after image url provided by VisualPing.

### In Terminal
make start

#### Run unit tests
make tests-unit

#### Run system tests
make tests-system

#### Hello world endpoint
http://127.0.0.1:8080/

#### Visual Ping Webhook
http://127.0.0.1:8080/listener/{context}