<?php

namespace App\Providers;

interface ReferenceInterface
{
    public function getReference(): string;

    public function setReference(string $reference): void;

    public function hasReference(): bool;
}
