<?php

namespace system\Unit\Middlewares;

use App\Middlewares\RecordMessage;
use App\Models\VisualPingInterface as VisualPingModelInterface;
use App\Providers\ReferenceInterface;
use App\Providers\Translators\VisualPingInterface;
use App\Services\Repository\CouchbaseInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Ramsey\Uuid\UuidFactoryInterface;

final class CouchbaseTest extends TestCase
{
    public function testMiddleware(): void
    {
        $sut = new RecordMessage(
            $this->createCouchbaseRepositoryInterface(),
            $this->createReferenceInterface(),
            $this->createVisualPingInterface(),
            $this->createResponseFactoryInterface(),
        );
        self::assertInstanceOf(MiddlewareInterface::class, $sut);
    }

    public function testMiddlewareSetsCouchbase(): void
    {
        $reference = 'test_reference';

        $referenceProvider = $this->createReferenceInterface();
        $referenceProvider->expects($this->once())
            ->method('getReference')
            ->willReturn($reference);
        $referenceProvider->expects($this->once())
            ->method('hasReference')
            ->willReturn(true);

        $visualPingModel = $this->createVisualPingModelInterface();

        $visualPingProvider = $this->createVisualPingInterface();
        $visualPingProvider->expects($this->once())
            ->method('getVisualPing')
            ->willReturn($visualPingModel);
        $visualPingProvider->expects($this->once())
            ->method('hasVisualPing')
            ->willReturn(true);

        $responseFactory = $this->createResponseFactoryInterface();
        $responseFactory->expects($this->never())
            ->method('createResponse');

        $couchbaseRepo = $this->createCouchbaseRepositoryInterface();
        $couchbaseRepo->expects($this->once())
            ->method('storeVisualPing')
            ->with($visualPingModel, $reference);

        $sut = new RecordMessage(
            $couchbaseRepo,
            $referenceProvider,
            $visualPingProvider,
            $responseFactory,
        );

        $sut->process($this->createServerRequestInterface(), $this->createHandlerInterface());
    }

    public function testMiddlewareReturns400NoReference(): void
    {
        $referenceProvider = $this->createReferenceInterface();
        $referenceProvider->expects($this->never())
            ->method('getReference');
        $referenceProvider->expects($this->once())
            ->method('hasReference')
            ->willReturn(false);

        $visualPingModel = $this->createVisualPingModelInterface();
        $visualPingModel->expects($this->never())
            ->method('getArray');

        $visualPingProvider = $this->createVisualPingInterface();
        $visualPingProvider->expects($this->never())
            ->method('getVisualPing');
        $visualPingProvider->expects($this->never())
            ->method('hasVisualPing');

        $responseFactory = $this->createResponseFactoryInterface();
        $responseFactory->expects($this->once())
            ->method('createResponse')
            ->with('400', 'ERROR: Reference not provided.')
            ->willReturn($this->createResponseInterface());

        $uuidFactory = $this->createUuidFactoryInterface();
        $uuidFactory->expects($this->never())
            ->method('uuid4');

        $couchbaseRepo = $this->createCouchbaseRepositoryInterface();
        $couchbaseRepo->expects($this->never())
            ->method('storeVisualPing');

        $sut = new RecordMessage(
            $couchbaseRepo,
            $referenceProvider,
            $visualPingProvider,
            $responseFactory,
        );

        $sut->process($this->createServerRequestInterface(), $this->createHandlerInterface());
    }

    public function testMiddlewareReturns400NoVisualPing(): void
    {
        $referenceProvider = $this->createReferenceInterface();
        $referenceProvider->expects($this->never())
            ->method('getReference');
        $referenceProvider->expects($this->once())
            ->method('hasReference')
            ->willReturn(true);

        $visualPingModel = $this->createVisualPingModelInterface();
        $visualPingModel->expects($this->never())
            ->method('getArray');

        $visualPingProvider = $this->createVisualPingInterface();
        $visualPingProvider->expects($this->never())
            ->method('getVisualPing');
        $visualPingProvider->expects($this->once())
            ->method('hasVisualPing')
            ->willReturn(false);

        $responseFactory = $this->createResponseFactoryInterface();
        $responseFactory->expects($this->once())
            ->method('createResponse')
            ->with('400', 'ERROR: Visual Ping model not provided.')
            ->willReturn($this->createResponseInterface());

        $uuidFactory = $this->createUuidFactoryInterface();
        $uuidFactory->expects($this->never())
            ->method('uuid4');

        $couchbaseRepo = $this->createCouchbaseRepositoryInterface();
        $couchbaseRepo->expects($this->never())
            ->method('storeVisualPing');

        $sut = new RecordMessage(
            $couchbaseRepo,
            $referenceProvider,
            $visualPingProvider,
            $responseFactory,
        );

        $sut->process($this->createServerRequestInterface(), $this->createHandlerInterface());
    }

    /**
     * @return UuidFactoryInterface|MockObject
     */
    private function createUuidFactoryInterface(): UuidFactoryInterface
    {
        return self::createMock(UuidFactoryInterface::class);
    }

    /**
     * @return ResponseFactoryInterface|MockObject
     */
    private function createResponseFactoryInterface(): ResponseFactoryInterface
    {
        return self::createMock(ResponseFactoryInterface::class);
    }

    /**
     * @return ReferenceInterface|MockObject
     */
    private function createReferenceInterface(): ReferenceInterface
    {
        return self::createMock(ReferenceInterface::class);
    }

    /**
     * @return CouchbaseInterface|MockObject
     */
    private function createCouchbaseRepositoryInterface(): CouchbaseInterface
    {
        return self::createMock(CouchbaseInterface::class);
    }

    /**
     * @return RequestHandlerInterface|MockObject
     */
    private function createHandlerInterface(): RequestHandlerInterface
    {
        return self::createMock(RequestHandlerInterface::class);
    }

    /**
     * @return ServerRequestInterface|MockObject
     */
    private function createServerRequestInterface(): ServerRequestInterface
    {
        return self::createMock(ServerRequestInterface::class);
    }

    /**
     * @return VisualPingInterface|MockObject
     */
    private function createVisualPingInterface(): VisualPingInterface
    {
        return self::createMock(VisualPingInterface::class);
    }

    /**
     * @return VisualPingModelInterface|MockObject
     */
    private function createVisualPingModelInterface(): VisualPingModelInterface
    {
        return self::createMock(VisualPingModelInterface::class);
    }

    /**
     * @return ResponseInterface|MockObject
     */
    private function createResponseInterface(): ResponseInterface
    {
        return self::createMock(ResponseInterface::class);
    }
}
