<?php

namespace unit\Factory;

use App\Factory\VisualPing;
use App\Factory\VisualPingInterface;
use App\Models\VisualPing as VisualPingModel;
use App\Models\VisualPingInterface as VisualPingInterfaceModel;
use PHPUnit\Framework\TestCase;

final class VisualPingTest extends TestCase
{
    public function testInterface(): void
    {
        $sut = new VisualPing();
        self::assertInstanceOf(VisualPingInterface::class, $sut);
    }

    public function testFactory(): void
    {
        $sut = new VisualPing();
        $model = $sut->create();

        self::assertInstanceOf(VisualPingInterfaceModel::class, $model);
        self::assertInstanceOf(VisualPingModel::class, $model);
    }
}
