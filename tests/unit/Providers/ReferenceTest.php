<?php

namespace Tests\Unit\Providers;

use App\Providers\Reference;
use App\Providers\ReferenceInterface;
use PHPUnit\Framework\TestCase;

final class ReferenceTest extends TestCase
{
    public function testInterface(): void
    {
        $sut = new Reference();
        self::assertInstanceOf(ReferenceInterface::class, $sut);
    }

    public function testProvider(): void
    {
        $test = 'testString';

        $sut = new Reference();
        self::assertFalse($sut->hasReference());

        $sut->setReference($test);
        self::assertTrue($sut->hasReference());
        self::assertSame($test, $sut->getReference());
    }
}
