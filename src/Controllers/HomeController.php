<?php

namespace App\Controllers;

use App\Factory\CouchbaseInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class HomeController
{
    public function __construct(private readonly CouchbaseInterface $couchbase)
    {
    }

    public function home(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        try {
            $this->couchbase->getCluster();
            echo "Couchbase Connection Success";
        } catch (\Throwable $e) {
            echo "Couchbase Connection Failure: " . $e->getMessage();
        }

        return $response;
    }
}
