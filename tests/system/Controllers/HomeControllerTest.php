<?php

namespace system\Controllers;

use GuzzleHttp\Client;
use Laminas\Diactoros\RequestFactory;
use PHPUnit\Framework\TestCase;

final class HomeControllerTest extends TestCase
{
    public function testHomePage(): void
    {
        $requestFactory = new RequestFactory();

        $client = new Client();
        $response = $client->send($requestFactory->createRequest('GET', 'http://web/'));
        self::assertSame(200, $response->getStatusCode());
        self::assertSame('Couchbase Connection Success', $response->getBody()->getContents());
    }
}
