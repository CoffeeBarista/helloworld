<?php

namespace App\Services\Translators;

use App\Models\VisualPingInterface as ModelVisualPingInterface;

interface VisualPingInterface
{
    public function translateRequest(array $data): ModelVisualPingInterface;
}
