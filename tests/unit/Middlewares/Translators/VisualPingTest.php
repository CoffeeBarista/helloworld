<?php

namespace unit\Middlewares\Translators;

use App\Middlewares\Translators\VisualPing;
use App\Models\VisualPingInterface as VisualPingModelInterface;
use App\Providers\Translators\VisualPingInterface as VisualPingProviderInterace;
use App\Services\Translators\VisualPingInterface as VisualPingTranslatorInterface;
use Laminas\Diactoros\ResponseFactory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class VisualPingTest extends TestCase
{
    public function testInterface(): void
    {
        $sut = new VisualPing(
            $this->createVisualPingInterface(),
            $this->createVisualPingProviderInterace(),
            $this->createResponseFactoryInterface(),
        );
        self::assertInstanceOf(MiddlewareInterface::class, $sut);
    }

    public function testMiddlewareSetsProvider(): void
    {
        $json = '{}';
        $model = $this->createVisualPingModelInterface();

        $translatorService = $this->createVisualPingInterface();
        $translatorService->expects($this->once())
            ->method('translateRequest')
            ->with([])
            ->willReturn($model);

        $provider = $this->createVisualPingProviderInterace();
        $provider->expects($this->once())
            ->method('setVisualPing')
            ->with($model);

        $responseFactory = $this->createResponseFactoryInterface();
        $responseFactory->expects($this->never())
            ->method('createResponse');

        $sut = new VisualPing(
            $translatorService,
            $provider,
            $responseFactory
        );

        $stream = $this->createStreamInterace();
        $stream->expects($this->once())
            ->method('getContents')
            ->willReturn($json);

        $request = $this->createServerRequestInterface();
        $request->expects($this->once())
            ->method('getBody')
            ->willReturn($stream);

        $handler = $this->createHandlerInterface();

        $sut->process($request, $handler);
    }

    public function testMiddlewareThrows404OnInvalidBody(): void
    {
        $translatorService = $this->createVisualPingInterface();
        $translatorService->expects($this->never())
            ->method('translateRequest');

        $provider = $this->createVisualPingProviderInterace();
        $provider->expects($this->never())
            ->method('setVisualPing');

        $responseFactory = $this->createResponseFactoryInterface();
        $responseFactory->expects($this->once())
            ->method('createResponse')
            ->willReturn((new ResponseFactory())->createResponse(404));

        $sut = new VisualPing(
            $translatorService,
            $provider,
            $responseFactory
        );

        $stream = $this->createStreamInterace();
        $stream->expects($this->once())
            ->method('getContents')
            ->willReturn('');

        $request = $this->createServerRequestInterface();
        $request->expects($this->once())
            ->method('getBody')
            ->willReturn($stream);

        $handler = $this->createHandlerInterface();

        $response = $sut->process($request, $handler);
        self::assertSame(404, $response->getStatusCode());
    }

    /**
     * @return VisualPingProviderInterace|MockObject
     */
    private function createVisualPingProviderInterace(): VisualPingProviderInterace
    {
        return self::createMock(VisualPingProviderInterace::class);
    }

    /**
     * @return VisualPingTranslatorInterface|MockObject
     */
    private function createVisualPingInterface(): VisualPingTranslatorInterface
    {
        return self::createMock(VisualPingTranslatorInterface::class);
    }

    /**
     * @return ResponseFactoryInterface|MockObject
     */
    private function createResponseFactoryInterface(): ResponseFactoryInterface
    {
        return self::createMock(ResponseFactoryInterface::class);
    }

    /**
     * @return VisualPingModelInterface|MockObject
     */
    private function createVisualPingModelInterface(): VisualPingModelInterface
    {
        return self::createMock(VisualPingModelInterface::class);
    }

    /**
     * @return RequestHandlerInterface|MockObject
     */
    private function createHandlerInterface(): RequestHandlerInterface
    {
        return self::createMock(RequestHandlerInterface::class);
    }

    /**
     * @return ServerRequestInterface|MockObject
     */
    private function createServerRequestInterface(): ServerRequestInterface
    {
        return self::createMock(ServerRequestInterface::class);
    }

    /**
     * @return StreamInterface|MockObject
     */
    private function createStreamInterace(): StreamInterface
    {
        return self::createMock(StreamInterface::class);
    }
}
