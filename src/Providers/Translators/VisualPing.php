<?php

namespace App\Providers\Translators;

use App\Models\VisualPingInterface as VisualPingModelInterface;

final class VisualPing implements VisualPingInterface
{
    private VisualPingModelInterface $visualPingModel;

    public function getVisualPing(): VisualPingModelInterface
    {
        return $this->visualPingModel;
    }

    public function setVisualPing(VisualPingModelInterface $model): void
    {
        $this->visualPingModel = $model;
    }

    public function hasVisualPing(): bool
    {
        return isset($this->visualPingModel);
    }
}
