<?php

namespace App\Controllers;

use App\Providers\ReferenceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListenerController
{
    public function __construct(private readonly ReferenceInterface $reference)
    {
    }

    public function listener(
        ServerRequestInterface $request,
        ResponseInterface $response,
    ): ResponseInterface {
        if ($this->reference->hasReference()) {
            echo $this->reference->getReference();
        }

        return $response;
    }
}
