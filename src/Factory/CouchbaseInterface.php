<?php

namespace App\Factory;

use Couchbase\Cluster;

interface CouchbaseInterface
{
    public function getCluster(): Cluster;
}
