<?php

namespace App\Services\Repository;

use App\Factory\CouchbaseInterface as CouchbaseFactoryInterface;
use App\Models\VisualPingInterface as ModelVisualPingInterface;
use Ramsey\Uuid\UuidFactoryInterface;

final class Couchbase implements CouchbaseInterface
{
    public function __construct(
        private readonly CouchbaseFactoryInterface $couchbaseFactory,
        private readonly UuidFactoryInterface $uuidFactory,
    ) {
    }

    public function storeVisualPing(
        ModelVisualPingInterface $visualPing,
        string $reference
    ): void {
        $couchbase = $this->couchbaseFactory->getCluster();
        $bucket = $couchbase->bucket('events');
        $scope = $bucket->scope('_default');
        $collection = $scope->collection('_default');

        $collection->upsert(
            $this->uuidFactory->uuid4(),
            [
                $reference => $visualPing->getArray(),
            ]
        );
    }
}
