<?php

use App\Controllers\HomeController;
use App\Controllers\ListenerController;
use App\Middlewares\RecordMessage;
use App\Middlewares\Reference;
use App\Middlewares\Translators\VisualPing as MiddlewareVisualPing;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../src/container.php';

AppFactory::setContainer($container);
$app = AppFactory::create();

// Routes
$app->get('/', [HomeController::class, 'home']);

$app->get(
    '/listener',
    function (RequestInterface $request, ResponseInterface $response) use ($container) {
        $responseFactory = $container->get(ResponseFactoryInterface::class);
        return $responseFactory->createResponse(404);
    }
);

$app->get('/listener/{context}', [ListenerController::class, 'listener'])
    ->add($container->get(RecordMessage::class))
    ->add($container->get(MiddlewareVisualPing::class))
    ->add($container->get(Reference::class));

$app->run();