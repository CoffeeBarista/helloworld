<?php

namespace App\Providers;

final class Reference implements ReferenceInterface
{
    private string $reference;

    public function getReference(): string
    {
        return $this->reference;
    }

    public function setReference(string $reference): void
    {
        $this->reference = $reference;
    }

    public function hasReference(): bool
    {
        return isset($this->reference);
    }
}
