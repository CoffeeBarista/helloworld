<?php

namespace App\Services\Repository;

use App\Models\VisualPingInterface;

interface CouchbaseInterface
{
    public function storeVisualPing(VisualPingInterface $visualPing, string $reference): void;
}
