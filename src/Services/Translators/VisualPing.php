<?php

namespace App\Services\Translators;

use App\Factory\VisualPingInterface as VisualPingFactoryInterface;
use App\Models\VisualPingInterface as ModelVisualPingInterface;

final class VisualPing implements VisualPingInterface
{
    public function __construct(private readonly VisualPingFactoryInterface $visualPingFactory)
    {
    }

    /**
     * @param array<string, string> $data
     */
    public function translateRequest(array $data): ModelVisualPingInterface
    {
        $model = $this->visualPingFactory->create();

        if (array_key_exists('url', $data)) {
            $model->setUrl($data['url']);
        }

        if (array_key_exists('preview', $data)) {
            $model->setPreviewView($data['preview']);
        }

        if (array_key_exists('original', $data)) {
            $model->setOriginalView($data['original']);
        }

        return $model;
    }
}

/**
 * {
 * "url": "https://www.visualping.io/",
 * "description": "Sample",
 * "datetime": "Tue Jul 25 2023 18:25:35 GMT+0000 (Coordinated Universal Time)",
 * "preview": "https://vp-files-ore.s3.amazonaws.com/resources/3months/GfMEUzFhUe5hzDVPr5HJK1R7-K0~QQV2567opQgsv9BE-QVfFHBjioo_diff.png",
 * "original": "https://s3.us-west-2.amazonaws.com/vp-files-ore/resources/3months/GfMEUzFhUe5hzDVPr5HJK1R7-K0.png",
 * "change": "1.0999999999999999 %",
 * "view_changes": "https://visualping.io/autologin?redirect=%2Fjobs%2F4270782%3Fmode%3Dtext",
 * "text_changes": "https://vp-files-ore.s3.amazonaws.com/resources/3months/GfMEUzFhUe5hzDVPr5HJK1R7-K0~QQV2567opQgsv9BE-QVfFHBjioo_diff.html",
 * "added_text": "New Text",
 * "removed_text": "Old Text"
 * }
 */
