<?php

namespace unit\Providers\Translators;

use App\Providers\Translators\VisualPing;
use App\Providers\Translators\VisualPingInterface;
use PHPUnit\Framework\TestCase;

final class VisualPingTest extends TestCase
{
    public function testInterface(): void
    {
        $sut = new VisualPing();
        self::assertInstanceOf(VisualPingInterface::class, $sut);
    }

    public function testProvider(): void
    {
        $model = new \App\Models\VisualPing();

        $sut = new VisualPing();
        self::assertFalse($sut->hasVisualPing());

        $sut->setVisualPing($model);
        self::assertTrue($sut->hasVisualPing());
        self::assertSame($model, $sut->getVisualPing());
    }
}
