<?php

namespace Tests\Unit\Middlewares;

use App\Middlewares\Reference;
use App\Providers\ReferenceInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class ReferenceTest extends TestCase
{
    public function testInterface(): void
    {
        $sut = new Reference(
            $this->createReferenceInterface(),
        );
        self::assertInstanceOf(MiddlewareInterface::class, $sut);
    }

    public function testMiddlewareSetsProvider(): void
    {
        $test = 'testString';

        $referenceProvider = $this->createReferenceInterface();
        $referenceProvider->expects($this->once())
            ->method('setReference')
            ->with($test);

        $sut = new Reference($referenceProvider);

        $request = $this->createServerRequestInterface();
        $request->expects($this->once())
            ->method('getRequestTarget')
            ->willReturn('/something/' . $test);

        $handler = $this->createHandlerInterface();

        $sut->process($request, $handler);
    }

    /**
     * @return ReferenceInterface|MockObject
     */
    private function createReferenceInterface(): ReferenceInterface
    {
        return self::createMock(ReferenceInterface::class);
    }

    /**
     * @return RequestHandlerInterface|MockObject
     */
    private function createHandlerInterface(): RequestHandlerInterface
    {
        return self::createMock(RequestHandlerInterface::class);
    }

    /**
     * @return ServerRequestInterface|MockObject
     */
    private function createServerRequestInterface(): ServerRequestInterface
    {
        return self::createMock(ServerRequestInterface::class);
    }
}
