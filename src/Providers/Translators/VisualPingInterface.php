<?php

namespace App\Providers\Translators;

use App\Models\VisualPingInterface as VisualPingModelInterface;

interface VisualPingInterface
{
    public function getVisualPing(): VisualPingModelInterface;

    public function setVisualPing(VisualPingModelInterface $model): void;

    public function hasVisualPing(): bool;
}
