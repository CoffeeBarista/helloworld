<?php

namespace App\Models;

interface VisualPingInterface
{
    public function setUrl(string $url): void;

    public function getUrl(): ?string;

    public function setOriginalView(string $url): void;

    public function getOriginalView(): ?string;

    public function setPreviewView(string $url): void;

    public function getPreviewView(): ?string;

    /**
     * @return array<mixed>
     */
    public function getArray(): array;
}
