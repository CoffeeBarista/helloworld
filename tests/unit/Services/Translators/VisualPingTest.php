<?php

namespace Tests\Unit\Services\Translators;

use App\Factory\VisualPingInterface as VisualPingFactoryInterface;
use App\Models\VisualPingInterface as VisualPingInterfaceModel;
use App\Services\Translators\VisualPing;
use App\Services\Translators\VisualPingInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class VisualPingTest extends TestCase
{
    public function testInterface(): void
    {
        $sut = new VisualPing($this->createVisualPingFactoryInterface());
        self::assertInstanceOf(VisualPingInterface::class, $sut);
    }

    public function testProvider(): void
    {
        $data = [
            'url' => $test = 'testString',
            'original' => $original = 'original',
            'preview' => $preview = 'preview',
        ];

        $model = $this->createVisualPingInterfaceModel();
        $model->expects($this->once())
            ->method('setUrl')
            ->with($test);
        $model->expects($this->once())
            ->method('setOriginalView')
            ->with($original);
        $model->expects($this->once())
            ->method('setPreviewView')
            ->with($preview);

        $factory = $this->createVisualPingFactoryInterface();
        $factory->expects($this->once())
            ->method('create')
            ->willReturn($model);

        $sut = new VisualPing($factory);
        $sut->translateRequest($data);
    }

    /**
     * @return VisualPingFactoryInterface|MockObject
     */
    public function createVisualPingFactoryInterface(): VisualPingFactoryInterface
    {
        return $this->createMock(VisualPingFactoryInterface::class);
    }

    /**
     * @return VisualPingInterfaceModel|MockObject
     */
    public function createVisualPingInterfaceModel(): VisualPingInterfaceModel
    {
        return $this->createMock(VisualPingInterfaceModel::class);
    }
}
