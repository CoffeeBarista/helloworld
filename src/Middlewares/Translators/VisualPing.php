<?php

namespace App\Middlewares\Translators;

use App\Providers\Translators\VisualPingInterface as VisualPingProviderInterace;
use App\Services\Translators\VisualPingInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class VisualPing implements MiddlewareInterface
{
    public function __construct(
        private readonly VisualPingInterface $visualPingTranslator,
        private readonly VisualPingProviderInterace $visualPingProvider,
        private readonly ResponseFactoryInterface $responseFactory,
    ) {
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler,
    ): ResponseInterface {
        try {
            $data = json_decode($request->getBody()->getContents(), true, JSON_THROW_ON_ERROR);

            if (is_null($data)) {
                throw new \Exception('Null Body');
            }
        } catch (\Throwable $e) {
            return $this->responseFactory->createResponse(400, 'ERROR: Expected Json Body');
        }

        $model = $this->visualPingTranslator->translateRequest($data);
        $this->visualPingProvider->setVisualPing($model);

        return $handler->handle($request);
    }
}
