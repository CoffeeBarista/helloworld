<?php

namespace App\Factory;

use Couchbase\Cluster;
use Couchbase\ClusterOptions;

final class Couchbase implements CouchbaseInterface
{
    public function getCluster(): Cluster
    {
        $options = new ClusterOptions();
        $options->credentials(getenv('COUCHBASE_USER'), getenv('COUCHBASE_PASSWORD'));
        $options->applyProfile("wan_development");
        return new Cluster("couchbase://" . getenv('COUCHBASE_HOST'), $options);
    }
}
