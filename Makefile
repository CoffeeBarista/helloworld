start:
	docker-compose down
	docker-compose build
	docker-compose up -d
	docker-compose run --rm composer install

stop:
	docker-compose down
	docker volume prune -f

tests-unit:
	docker-compose exec web ./vendor/bin/phpunit tests/unit

tests-system:
	docker-compose exec web ./vendor/bin/phpunit tests/system

phpstan:
	docker-compose exec web ./vendor/bin/phpstan analyse src tests

phpcs:
	docker-compose exec web ./vendor/bin/phpcs --standard=PSR12 src tests
