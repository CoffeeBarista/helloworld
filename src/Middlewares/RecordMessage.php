<?php

namespace App\Middlewares;

use App\Providers\ReferenceInterface;
use App\Providers\Translators\VisualPingInterface;
use App\Services\Repository\CouchbaseInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class RecordMessage implements MiddlewareInterface
{
    public function __construct(
        private readonly CouchbaseInterface $couchbaseFactory,
        private readonly ReferenceInterface $referenceProvider,
        private readonly VisualPingInterface $visualPingProvider,
        private readonly ResponseFactoryInterface $responseFactory,
    ) {
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler,
    ): ResponseInterface {
        if (!$this->referenceProvider->hasReference()) {
            return $this->responseFactory->createResponse(400, 'ERROR: Reference not provided.');
        }

        if (!$this->visualPingProvider->hasVisualPing()) {
            return $this->responseFactory->createResponse(400, 'ERROR: Visual Ping model not provided.');
        }

        $this->couchbaseFactory->storeVisualPing(
            $this->visualPingProvider->getVisualPing(),
            $this->referenceProvider->getReference(),
        );

        return $handler->handle($request);
    }
}
