<?php

namespace system\Services\Repository;

use App\Factory\Couchbase;
use App\Factory\CouchbaseInterface;
use App\Models\VisualPingInterface as VisualPingModelInterface;
use App\Services\Repository\Couchbase as CouchbaseRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidFactoryInterface;

final class CouchbaseTest extends TestCase
{
    public function testRepositorySetsModelData(): void
    {
        $reference = 'test_reference';
        $modelData = ['test_stuff' => 'stuff'];
        $uuid = Uuid::fromString('d3e2a064-2d29-4945-a982-fd6096bc659f');

        $visualPingModel = $this->createVisualPingModelInterface();
        $visualPingModel->expects($this->once())
            ->method('getArray')
            ->willReturn($modelData);

        $uuidFactory = $this->createUuidFactoryInterface();
        $uuidFactory->expects($this->once())
            ->method('uuid4')
            ->willReturn($uuid);

        $sut = new CouchbaseRepository(
            $couchbase = $this->createCouchbase(),
            $uuidFactory,
        );

        $sut->storeVisualPing($visualPingModel, $reference);

        $bucket = $couchbase->getCluster()->bucket('events');
        $scope = $bucket->scope('_default');
        $collection = $scope->collection('_default');

        $data = $collection->get($uuid->toString());
        self::assertSame([
            $reference => $modelData
        ], $data->content());
    }

    /**
     * @return UuidFactoryInterface|MockObject
     */
    private function createUuidFactoryInterface(): UuidFactoryInterface
    {
        return self::createMock(UuidFactoryInterface::class);
    }

    private function createCouchbase(): CouchbaseInterface
    {
        return new Couchbase();
    }

    /**
     * @return VisualPingModelInterface|MockObject
     */
    private function createVisualPingModelInterface(): VisualPingModelInterface
    {
        return self::createMock(VisualPingModelInterface::class);
    }
}
